""" 
Ввод и вывод

Стандартный поток ввода
stdin (№ 0) -> input()
sys.stdin

Стандартный поток вывода
stdout (№ 1) -> print()
sys.stdout

Стандартный поток ошибок
stderr (№ 2) -> ошибки интерпретатора
sys.stderr
"""


""" 
Работа с файлами

open(filename[, mode]) - путь к файлу (абс или отн) и режим открытия (по ум-ю 'r')

Режимы записи:
w - перезапись (write)
a - дозапись в конец (append)
x - эксклюзивное создание файла (создается, если не существут. если сущ -> ошибка)

Режимы чтения:
r - чтение (read) (по-умолчанию)

w+, a+, x+ - чтение + запись

===
t - текстовый режим (по-умолчанию) -> open('out.txt', 'wt')
b - бинарный режим -> open('sound.mp3', 'rb')

"""

# Открываем файл в режиме записи

f = open('out.txt', 'w')
f.write("рок hello")
f.write('\rback')
f.write("456\n")
f.writelines(['line', 'newline'])
f.close()

# Открывает файл в режиме чтения

f = open('out.txt', 'r')  # то же, что и open('out.txt', 'r')
print('Как прочитать файл целиком в строку\n')
content = f.read()  # на этом этапе курсор перемещается в конец и уже нич. не читается
print(content)

f.seek(0)  # переместить курсор на 0 байт от начала файла

print('Как прочитать файл списком?\n')
content = f.readlines()
print(content)

f.seek(0)

print('Как прочитать файл построчно\n')
line = f.readline()
print(line)

for line in f:
	print(line.strip())

f.seek(0)

print("Как прочитать из файла N байтов\n")
print(f.read(2))

print("Как узнать позицию курсора\n")
print(f.tell())

f.close()

print('Менеджер контекста\n')
# Менеджер контекста

with open('out.txt') as f2:
	content2 = f2.read()
	print(content2)
