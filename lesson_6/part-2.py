# Форматы данных

# Pickle

import pickle  # python2 =( -> six

data = {
    'users': [
        {
            'id': 1,
            'name': 'Linux Torvalds',
            'skills': ('C++', 'C')
        },
        {
            'id': 2,
            'name': 'Richard Stallman',
            'skills': ('C', 'GNU')
        }
    ]
}

with open('users.pickle', 'wb') as f:
    pickle.dump(data, f)

with open('users.pickle', 'rb') as f:
    print(pickle.load(f))

# JSON - JavaScript Object Notation

import json

with open('users.json', 'w') as f:
    json.dump(data, f, indent=4)

with open('users.json', 'r') as f:
    print(json.load(f))

# CSV

import csv

"""
строка - запись, поля разделены разделителем
можем задать названия полей первой строкой

id;name;skills
1;Linus Torvalds;C,C++
2;Richard Stallman;C++,GNU
"""

with open('users.csv', 'w') as f:
    # метод get() выдает None, если ключ отсутствует. можно задать по-умолчанию
    users = data.get('users', [])
    field_names = users[0].keys()  # Сперва проверить, есть ли там элемент

    writer = csv.DictWriter(f, fieldnames=field_names)  # Врайтер для словарей
    writer.writeheader()

    for user in users:
        writer.writerow(user)  # или writerows(users) вместо цикла

with open('users.csv') as f:
    reader = csv.DictReader(f)

    for row in reader:
        print(row)

# Остальные форматы
# ini, conf
# xml (модуль lxml для парсинга xml и html)
# и др

# =========================================
"""
Базы данных
"""

# SQLite3 - База данных в одном файле


