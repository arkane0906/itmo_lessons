# todo: Итераторы

s = 'Linus Torvalds'
lst = [1, 2, 3, 4, 5]
person = {
	'name': 'Linus Torvalds',
	'age': '47',
	'is_developer': True
}

# it = iter(person)
# print(next(it))
# print(next(it))
# print(next(it))

# for i in s:
# 	print(i)

# it = iter(s)
# while True:
# 	try:
# 		i = next(it)
# 		print(i)
# 	except StopIteration:
# 		break


"""
.keys() - возвращает "список" ключей словаря
.values() - возвращает "список" значений словаря
.items() - возвращает "список" кортежей "(ключ, значение)" словаря
"""

# for key, value in person.items():
# 	print(key, value)

# print(list(enumerate(lst)))

# for i, value in enumerate(lst):
# 	print(i, value)



# todo: Генераторы

"""
Выполняется до yield, по методу next до след yield
т.е. запоминает положение
Основная цель - экономить память
"""

# def generator():
# 	print('Шаг №1')
# 	yield 1
# 	print('Шаг №2')
# 	yield 2
# 	print('Шаг №3')

# gen = generator()
# print(type(gen))
# print(next(gen))
# print(next(gen))

# for i in generator():
# 	print(i)

# def countdown(n):
# 	print('Генератор запустился')

# 	while n > 0:
# 		yield n
# 		n -= 1

# 	print('Отсчет завершен')

# for i in countdown(5):
# 	print(i)



# todo: Генераторы списков/словарей/множеств

"""
[expression for item1 in iterable if conditional1
            for item2 in iterable if conditional2
            ...
            for itemN in iterable if conditionalN]
"""

numbers = [x for x in range(10)]
# print(numbers)

odd = [x for x in numbers if x % 2]
# print(odd)

even = [x for x in numbers if not x % 2]
# print(even)

# points = []
# for x in range(3):
# 	for y in range(3):
# 		points.append((x, y))

points = [(x, y) for x in range(3) for y in range(3)]
# print(points)


lst = [1, 1, 2, 1, 3, 2, 3, 3, 1]
s = {i for i in lst}
# print(s)

keys = ['id', 'original_url', 'short_url']
values = [1, 'http://python.org', '/1']
data = {k: v for i, k in enumerate(keys)
             for j, v in enumerate(values)
             if i == j}  # никогда так не делать)


data = [
	{
		'id': 1,
		'name': 'Linus Torvalds',
		'is_developer': True
	},
	{
		'id': 2,
		'name': 'Richard Stallman',
		'is_developer': True
	},
	{
		'id': 1,
		'name': 'Linus Torvalds',
		'is_developer': True
	}
]
# Выбираем ключ (тут id) и записываем в него данные,
# если дублируется ключ - перезаписывается
persons = {d['id']: d for d in data}
# print(persons)



# todo: Выражения-генераторы

points2 = ((x, y) for x in range(3) for y in range(3))
# print(points2)

with open(__file__) as f:
	lines = (line.strip() for line in f)
	todos = (x.split(': ').pop() for x in lines if x.startswith('# todo:'))
	# print(list(todos))



# todo: Сопрограммы

def coroutine(func):
	def wrapper(*args, **kwargs):
		g = func(*args, **kwargs)
		next(g)
		return g
	return wrapper

@coroutine
def splitter(delimiter=None):
	print('Генератор готов к приему данных')
	result = None
	while True:
		print('wait...')
		s = (yield result)
		print(s)
		result = s.split(delimiter)

sp = splitter(', ')

print(sp.send('A, B, C'))
print(sp.send('1, 2, 3'))

# sp.close() - закрывает супрограмму (возбуждает исключение GeneratorExit)
# sp.throw(RuntimeError, 'Тебе капец') - эквивалент raise в сопраграмме
