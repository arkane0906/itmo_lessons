# Конвертер валют

import sys
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QWidget, QLabel, QDoubleSpinBox, 
    QPushButton, QVBoxLayout
)

"""
Можно загрузить UI, созданный в QtDesigner с помощью:
from PyQt5.uic import loadUi

loadUi('path/to/file/main_window.ui', self)  (внутри QMainWindow)

можно скомпилить с помощью pyuic5 (посмотерть)
"""


class Course(QObject):
    @staticmethod
    def get():
        return 59.407117


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initUi()
        self._initSignals()
        self._initLayouts()

    def _initUi(self):
        self.setWindowTitle('Конвертер валют')

        self.sourceLabel = QLabel('Сумма в рублях', self)
        self.sourceAmount = QDoubleSpinBox(self)
        self.sourceAmount.setMaximum(999999999)

        self.resultLabel = QLabel('Сумма в долларах', self)
        self.resultAmount = QDoubleSpinBox(self)
        self.resultAmount.setMaximum(999999999)

        self.convertButton = QPushButton('Перевести', self)
        self.clearButton = QPushButton('Очистить', self)

    def _initSignals(self):
        self.convertButton.clicked.connect(self.onConvert)
        self.clearButton.clicked.connect(self.onClear)

        self.sourceAmount.valueChanged.connect(self.updateState)
        self.resultAmount.valueChanged.connect(self.updateState)

        self.updateState()

    def _initLayouts(self):
        w = QWidget()

        self.mainLayout = QVBoxLayout(w)

        self.mainLayout.addWidget(self.sourceLabel)
        self.mainLayout.addWidget(self.sourceAmount)
        self.mainLayout.addWidget(self.resultLabel)
        self.mainLayout.addWidget(self.resultAmount)
        self.mainLayout.addWidget(self.convertButton)
        self.mainLayout.addWidget(self.clearButton)

        self.setCentralWidget(w)

    def onConvert(self):
        if self.validate():
            course = Course.get()
            if self.sourceAmount.value():
                self.resultAmount.setValue(
                    self.sourceAmount.value() / course
                )
            else:
                self.sourceAmount.setValue(
                    self.resultAmount.value() * course
                )

    def onClear(self):
        self.sourceAmount.setValue(0) 
        self.resultAmount.setValue(0)

    def updateState(self):
        self.convertButton.setEnabled(self.validate())

    def validate(self):
        return bool(self.sourceAmount.value()) ^ \
               bool(self.resultAmount.value()) # xor !(both True or both False)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
