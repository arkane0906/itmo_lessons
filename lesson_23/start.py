# PyQt5

import sys
from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QPushButton
)

class Start(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._increment_size = True # Увеличивать размер?

        self._initUi()
        self._initSignals()

    def _initUi(self):
        self.resize(200, 300)
        self.setMinimumSize(190, 200)
        self.setMaximumSize(210, 400)

        self.btn = QPushButton('Кнопка', self)
        self.setCentralWidget(self.btn)

    def _initSignals(self):
        self.btn.clicked.connect(self.onClick)

    def onClick(self):
        """Слот (обработчик сигнала)"""
        if self._increment_size:
            self.resize(self.width() + 10, self.height() + 10)
        else:
            self.resize(self.width() - 10, self.height() - 10)

        if self.width() <= self.minimumWidth():
            self._increment_size = True
        if self.width() >= self.maximumWidth():
            self._increment_size = False


if __name__ == '__main__':
    app = QApplication(sys.argv)

    w = Start()
    w.show()

    sys.exit(app.exec_())
