class Person(object):
    def __init__(self):
        self.username = None


person = Person()
# print(person.username)  # None
person.username = 'Новое имя'
# print(person.username)  # 'Новое имя'

# print(person.lastname)  # AttributeError
person.lastname = 'Новая фамилия'
# print(person.lastname)  # 'Новая фамилия'

"""
hasattr(obj, attr)            # Проверяет, что атрибут существует
getattr(obj, attr, default*)  # Получает знаечние атрибута
setattr(obj, attr, val)       # Устанавливает значение атрибута
delattr(obj, attr)            # Удаляет атрибут
"""

# print(hasattr(person, 'age'))  # False
# print(getattr(person, 'age'))  # False
# print(getattr(person, 'age', False))  # person.age or False
