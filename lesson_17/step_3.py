# Специальные свойства и метода классов и объектов

class Singleton(object):
    _instances = {}

    def __new__(cls, *args, **kwargs):
        """Конструктор, вызывается для создания объекта"""
        if cls not in cls._instances:
            cls._instances[cls] = super().__new__(cls, *args, **kwargs)
        return cls._instances[cls]


class Config(Singleton):
    def __init__(self):
        self.__config = {}

    def add(self, key, value):
        self.__config[key] = value

    def get(self, key):
        return self.__config.get(key)

config1 = Config()
config2 = Config()
# print(config1 is config2)  # True

config2.add('host', 'localhost')
# print(config1.get('host'))  # localhost

# ===============


class Product(object):
    """Продукт магазина"""

    def __init__(self, title, price):
        # не станем закрывать, т.к. пример (self.__title)
        self.title = title
        self.price = price

    def get_price(self):
        return self.price

    def __str__(self):
        return "Книга '{}' со стоимостью {}".format(self.title, self.price)

    def __repr__(self):
        """Вызывается функцией repr"""
        return "Product('{}', {})".format(self.title, self.price)

    def __int__(self):
        return int(self.price)

    def __float__(self):
        return float(self.price)

    def __bool__(self):
        return bool(self.title and self.price)

# Методы класса

# print(Product.__name__)  # Имя класса
# print(Product.__class__)  # Метакласс
# print(Product.__module__)  # Имя модуля, в котором описан класс
# print(Product.__doc__)  # Строка документации
# print(Product.__bases__)  # Кортеж базовых классов
# print(Product.__dict__)  # Словарь атрибутов

p = Product('Книга по ООП', 666.6)

# Методы объекта

# print(p.__dict__)  # Словарь атрибуторв объекта
# print(p.__class__)  # Класс объекта
# print(p.__class__.__name__)  # Имя класса объекта

# print(p)
# print(repr(p))
# print(eval(repr(p)))

# print(int(p), float(p), bool(p))

class Cart(object):
    """Коллекция продуктов"""
    def __init__(self):
        self.__products = []

    def add(self, product):
        if not isinstance(product, Product):
            raise TypeError
        self.__products.append(product)

    def __len__(self):
        return len(self.__products)

    def __contains__(self, product):
        assert isinstance(product, Product)
        return product in self.__products

    def __setitem__(self, key, product):
        """obj[0] = ... """
        assert isinstance(product, Product)
        self.__products.insert(key, product)

    def __getitem__(self, key):
        """obj[0]"""
        return self.__products[key]

    def __delitem__(self, key):
        """del obj[0]"""
        del self.__products[key]


# cart = Cart()

# product1 = Product('Книга по ООП', 666.6)
# cart.add(product1)

# product2 = Product('Книга по SQL', 300)
# cart.add(product2)

# product3 = Product('Кофе', 85)

# cart[5] = product3

# print('Кол-во товаров:', len(cart))
# print(product1 in cart)



# Перегрузка операторов


class Vector(object):
    __slots__ = ('x', 'y')

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Vector({}, {})'.format(self.x, self.y)

    def __add__(self, other):
        """Перегрузка оператора + """
        assert isinstance(other, self.__class__)
        return self.__class__(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """Перегрузка оператора - """
        assert isinstance(other, self.__class__)
        return self.__class__(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        """Перегрузка оператора * """
        assert isinstance(other, self.__class__)
        return self.__class__(self.x * other.x, self.y * other.y)

    def __gt__(self, other):
        assert isinstance(other, self.__class__)
        return self.length > other.length

    def __ge__(self, other):
        assert isinstance(other, self.__class__)
        return self.length >= other.length

    def __eq__(self, other):
        assert isinstance(other, self.__class__)
        return self.length == other.length

    @property
    def length(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5



v1 = Vector(-3, 4)
v2 = Vector(-3, 6)

v3 = v1 + v2

v4 = v1 - v2

v5 = v1 * v2

print(v1)
print(v2)
print(v3)
print(v4)
print(v5)
print('Вектор v1 > v2:', v1 > v2)
print('Вектор v1 < v2:', v1 < v2)
print('Вектор v1 == v1:', v1 == v1)
print('Вектор v1 >= v1:', v1 >= v1)
