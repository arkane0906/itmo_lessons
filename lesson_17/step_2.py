Person = type('Person', (object,), {
    'firstname': None,
    'lastname': None
})

def __init__(self, firstname, lastname):
    self.firstname = firstname
    self.lastname = lastname

setattr(Person, '__init__', __init__)

# print(Person, type(Person))

person = Person('Вася', 'Карпов')

# print(person, type(person), dir(person))


# Метаклассы

class Singleton(type):
    """
    Синглтон - метакласс, позволяющий создавать только один
    экземпляр класса
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


class Config(metaclass=Singleton):
    def __init__(self):
        self.__config = {}

    def add(self, key, value):
        self.__config[key] = value

    def get(self, key):
        return self.__config.get(key)

config1 = Config()
config2 = Config()
print(config1 is config2)  # True

config2.add('host', 'localhost')
print(config1.get('host'))  # localhost
