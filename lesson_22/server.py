# Сокеты

from datetime import datetime
import argparse
import socket

from config import Config
import morse


"""
Создание Ethernet TCP (проверяет доставку пакетов) сокета
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    # Установка опций сокета
    # по типу: s.setsockopt(level, optname, value: int)
    s.set.sockopt(socket.SOL_SOCKET, SO_REUSEADDR, 1)

    # Привязка сокета к адресу (ip, port)
    s.bind((ip, port))

    # Прослушивание входящих соединений
    s.listen(10)  # кол-во одновременных соединений

    conn, addr = s.accept()
    # conn - сокет клиента (того, кто подключился)
    # addr - адрес клиента (ip, port)

"""


class TechnoCubeServer(object):
    def __init__(self, config):
        self.__config = Config(config)

    def configure(self):
        pass

    def listen(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind((self.__config.ip, self.__config.port))
            s.listen(1)

            while 1:
                conn, addr = s.accept()

                with conn:
                    msg = conn.recv(8192)  # 8kb
                    msg = msg.decode('utf-8').strip()

                    self.log(addr, msg)

                    if msg:
                        msg = morse.encode(msg)
                        conn.send(msg.encode('utf-8'))

    def log(self, addr, msg):
        template = '[{:%Y-%m-%d %H:%M:%S}] {} {}'
        print(template.format(datetime.now(), addr, msg))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Сервер Морзе'
    )

    parser.add_argument('-c', '--config', 
        help='Конфигурационный файл', default='config.ini')

    subparsers = parser.add_subparsers()

    parser_init = subparsers.add_parser(
        'init',
        help='Инициализация',
        description='Поиск платы Arduino и создание конфига'
    )

    parser_init.set_defaults(callback=lambda: server.configure())

    parser_server = subparsers.add_parser(
        'listen',
        help='Запуск сервера',
        description='Запуск сервера '
    )

    parser_server.set_defaults(callback=lambda: server.listen())

    arguments = vars(parser.parse_args())

    print(arguments)

    # config = arguments.pop('config')  # Внизу добавил сразу в класс

    server = TechnoCubeServer(arguments.pop('config'))

    if 'callback' in arguments:
        callback = arguments.pop('callback')
        callback(**arguments)
    else:
        parser.print_help()
