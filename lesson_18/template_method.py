# Template method - Шаблонный метод
# Поведенческий шаблон

from abc import ABCMeta, abstractmethod


class Command(metaclass=ABCMeta):

    @abstractmethod
    def _do_execute(self):
        """Конкретная реализация"""
        pass

    def execute(self):
        """Template method, Алгоритм"""
        print('Действие до выполнения команды')
        self._do_execute()
        print('Действие после выполнения команды')


class ListCommand(Command):

    def _do_execute(self):
        print('Показать список задач')


cmd = ListCommand()
cmd.execute()
