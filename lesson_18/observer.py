# Observer - Наблюдатель
# Publisher/Subscriber (Pub/Sub)
# Поведенческий шаблон

from abc import ABCMeta, abstractmethod
from random import randrange


class Event(object):

    def __init__(self, event_name):
        self._event_name = event_name

    @property
    def name(self):
        return self._event_name


class Subject(object):

    def __init__(self):
        self._observers = {}

    def add_observer(self, event_name, observer):
        assert event_name
        assert isinstance(observer, Observer)

        if event_name not in self._observers:
            self._observers[event_name] = []

        self._observers[event_name].append(observer)

    def remove_observer(self, event_name, observer):
        assert event_name
        assert isinstance(observer, Observer)

        if event_name in self._observers:
            self._observers[event_name].remove(observer)

    def notify_observers(self, event):
        assert isinstance(event, Event)
        event_name = event.name

        if event_name in self._observers:
            for observer in self._observers[event_name]:
                observer.handle_event(event)


class Observer(metaclass=ABCMeta):
    
    @abstractmethod
    def handle_event(self, event):
        pass


class Login(Subject):

    def __init__(self):
        super().__init__()
        self.result = None

    def authorize(self, user, password):
        """
        0 - Успешная авторизация
        1 - Неверный логин или пароль
        2 - Ошибка провайдера Ростелеком
        """
        self.result = randrange(3)

        if self.result == 0:
            event = Event('success')
        else:
            event = Event('error')
        event.result = self.result
        self.notify_observers(event)


class LoggerObserver(Observer):

    def handle_event(self, event):
        print('Пишем в access.log')


class ErrorObserver(Observer):

    def handle_event(self, event):
        if event.result == 1:
            print('Пишем в error.log')


class CookieObserver(Observer):

    def handle_event(self, event):
        if event.result == 2:
            print('Посылаем Cookie')


class LoginEvent(Event):

    def __init__(self, event_name, result):
        super().__init__(event_name)
        self.result = result


# Создаем субъект
login_handler = Login()  # Субъект (Observable)
# Подключаем наблюдателей
login_handler.add_observer('success', LoggerObserver())  # Наблюдатель (Observer)
login_handler.add_observer('error', ErrorObserver())   # Наблюдатель (Observer)
login_handler.add_observer('error', CookieObserver())  # Наблюдатель (Observer)


# Можем обрабатывать вход
login_handler.authorize('22', '33')
