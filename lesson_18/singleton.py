# Шаблоны проектирование (Design Patterns)

"""
Шаблон:
1) Имя
2) Проблема
    - Решение "в лоб"
3) Решение (с помощью шаблона)
4) Выводы (плюсы и минусы)

*) Участники (классы и отношения), UML (модель)


Группы шаблонов (условно):
1) Порождающие
2) Структурные
3) Поведенческие

"""

# Singleton - Одиночка
# Пораждающий шаблон проектирования
# Проблема: В системе может быть только один экз. класса


def singleton(cls):
    __instances = {}

    # функция wrapper
    def get_instance(*args, **kwargs):
        if cls not in __instances:
            __instances[cls] = cls(*args, **kwargs)
        return __instances[cls]

    return get_instance
    

@singleton
class Config(object):
    def __init__(self):
        self.__config = {}

    def __getitem__(self, key):
        return self.__config[key]

    def __setitem__(self, key, value):
        self.__config[key] = value


