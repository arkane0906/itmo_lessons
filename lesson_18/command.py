# Command/Action - Команда/Действие
# Поведенческий шаблон
# Цель: Позволяет инкапсулировать запрос
# на выполнение определенного действия
# в виде отдельного объекта

from abc import ABCMeta, abstractmethod


class Command(metaclass=ABCMeta):

    @abstractmethod
    def execute(self):
        pass


class ShowCommand(Command):

    def __init__(self, task_id):
        self.task_id = task_id

    def execute(self):
        print('Показать задачу с ID: {}'.format(self.task_id))


class Menu(object):

    def __init__(self):
        self.__commands = {}

    def add_command(self, name, command):
        if not name:
            raise ValueError
        if not issubclass(command, Command):
            raise TypeError
        self.__commands[name] = command

    def execute(self, name, *args, **kwargs):
        command = self.__commands.get(name)
        if not command:
            raise ValueError
        return command(*args, **kwargs).execute()


menu = Menu()
menu.add_command('show', ShowCommand)
menu.execute('show', 32)
