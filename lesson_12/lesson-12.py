# Классы и объекты

"""
Свойства - переменные, объявленные в контексте класса
         - данные
Метод - функция, объявленная в контексте класса
      - поведение
"""


class Person(object):

    def __init__(self, firstname, lastname):
        """
        Метод-Конструктор
        Основная цель - инициализировать объект
        self - ссылка на текущий экземпляр
        """

        self.firstname = firstname
        self.lastname = lastname

    def print_info(self):
        print('{} {}'.format(self.firstname, self.lastname))


person_1 = Person('Олег', 'Тиньков')  # Создание экземпляра
# print(person_1.firstname, person_1.lastname)


class Developer(Person):
    """Разработчик"""
    def __init__(self, firstname, lastname, skills):
        super().__init__(firstname, lastname)
        self.skills = skills

    def print_info(self):
        super().print_info()
        print('Скиллы: {}'.format(self.skills))


dev1 = Developer('Linus', 'Torvalds', ['linux', 'C'])
# dev1.print_info()



class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_x_y(self):
        return self.x, self.y

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def set_x_y(self, x, y):
        self.x, self.y = x, y

point = Point(10, 20)
# print(point.get_x(), point.get_y(), point.get_coords())

class OrderException(Exception):
    pass

class ProductException(Exception):
    pass


class Product(object):
    def __init__(self, name, price):
        self.name = name
        self.price = None  # не обязательно
        self.set_price(price)

    def set_price(self, price):
        if price <= 0:
            raise ProductException('Invalid price')
        self.price = price

    def get_price(self):
        return self.price


class Order(object):
    """ Заказ """
    def __init__(self, person):
        super().__init__()
        self.person = person
        self.products = []

    def add_product(self, product):
        if not isinstance(product, Product):
            raise OrderException(
                'Must be "Product" not "{}"'.format(type(product))
            )
        self.products.append(product)
        
    def get_total_price(self):
        return sum([p.get_price() for p in self.products])


prod1 = Product('Хлеб', 15)
prod2 = Product('Масло', 35)

order = Order(dev1)
order.add_product(prod1)
order.add_product(prod2)
# print(order.get_total_price())


# Статические свойства и методы

class Factory(object):
    instances = {}

    # @staticmethod
    @classmethod
    def get_instance(cls, name, *args, **kwargs):
        if name not in cls.instances:
            cls.instances[name] = cls(*args, **kwargs)
        return cls.instances[name]


print(
    Factory.get_instance('name1'),
    Factory.get_instance('name2'),
    Factory.get_instance('name1')
)
