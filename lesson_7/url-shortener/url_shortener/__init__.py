import os.path as Path
import sys
from collections import OrderedDict, namedtuple

from url_shortener import storage


get_connection = lambda: storage.connect('shortener.sqlite')


Action = namedtuple('Action', ['func', 'title'])
actions = OrderedDict()


def menu_action(cmd, title):
    def decorator(func):
        actions[cmd] = Action(func=func, title=title)
        return func
    return decorator


@menu_action('1', 'Добавить URL-адрес')
def action_add():
    """
    Добавить URL
    """

    url = input('\nВведите URL-адрес: ')

    if not url:
        return

    with get_connection() as conn:
        short_url = storage.add_url(conn, url)

    print('Короткий адрес: {}'.format(short_url))


@menu_action('2', 'Найти оригинальный URL-адрес')
def action_find():
    """
    Искать URL
    """

    short_url = input('\nВведите короткий URL-адрес: ')

    if short_url:
        with get_connection() as conn:
            row = storage.find_url_by_shorl_url(conn, short_url)

        if row:
            url = row.get('original_url')
            print('Оригинальынй URL-адрес: {}'.format(url))
        else:
            print('URL-адрес "{}" не найден'.format(short_url))


@menu_action('3', 'Вывести все URL-адреса')
def action_find_all():
    """
    Вывести все URL
    """

    with get_connection() as conn:
        rows = storage.find_all(conn)

    template = '{row[short_url]} - {row[original_url]} - {row[created]}'

    for row in rows:
        print(template.format(row=row))


@menu_action('m', 'Показать меню')
def action_show_menu():
    """
    Показать меню
    """
    menu = []

    for cmd, action in actions.items():
        menu.append('{}. {}'.format(cmd, action.title))
    print('\n'.join(menu))


@menu_action('q', 'Выход')
def action_exit():
    """
    Выйти из программы
    """
    sys.exit(0)


def main():
    # Определяем абсолютный путь к файлу со схемой БД
    creation_schema = Path.join(
        Path.dirname(__file__), 'schema.sql'
    )

    # Инициализируем БД
    with get_connection() as conn:
    	storage.initialize(conn, creation_schema)

    action_show_menu()

    while True:
        cmd = input('\nВведите команду: ')
        action = actions.get(cmd)  # Возвращает None если нет ком-ы

        if action:
            action.func()
        else:
            print('Неизвестная команда')
