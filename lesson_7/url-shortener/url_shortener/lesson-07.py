"""
SQL - Structured Query Language
- DDL - Data Definition Language
	(CREATE TABLE)
- DML - Data Manipulation Language
	(SELECT, INSERT, UPDATE, DELETE)

СУБД - Система Управления Базами Данных

Primary Key (Первичный ключ) - Уникальный идентификатор
	Может быть int, str, составной (суррогатный)
	
Foreign Key (Внешний ключ)

Алгоритм работы с БД:
1. Установка соединения .connect()
2. Создание объекта курсора conn.cursor()
3. Выполнение SQL запросов cursor.execute()
4. Если запрос изменяет данные/структуру,
   то зафиксировать изменения conn.commit()
4. Если запрос на выборку/получение данных
   то разобрать данные (fetch*) 
"""


# SQLite3

import sqlite3

# conn = sqlite3.connect(':memory:')
conn = sqlite3.connect('users.sqlite')
cursor = conn.cursor()

sql = """
	CREATE TABLE IF NOT EXISTS user (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		firstname TEXT NOT NULL,
		lastname TEXT NOT NULL,
		created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	)
"""

cursor.execute(sql)
conn.commit()

sql = """
	INSERT INTO user (
		firstname, lastname
	) VALUES (
		?, ?
	)
"""

cursor.execute(sql, ('Eugene', 'Idelbaev'))
conn.commit()

sql = """
	SELECT 
		id, firstname, lastname, created 
	FROM 
		user
"""

cursor.execute(sql)
users = cursor.fetchall()

conn.close()


# Используя контекстный менеджер все можно сократить

with sqlite3.connect('users.sqlite') as conn:
	cursor = conn.execute(sql)
	users = cursor.fetchall()
	print(users)
