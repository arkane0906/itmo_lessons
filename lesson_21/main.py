# Модульное тестирование
# unittest

import unittest


"""
Test Fixture - Испытательный стенд (окружение)
    Методы класса TestCase:
    - setUp()/tearDown()
    - setUpClass()/tearDownClass()
    - setUpModule()/tearDownModule()



Test Case - Тестовый случай (состоит из тестов) состоит из тестов (test_*)

Test Suite - Тестовый набор (состоит из TestCase)

Test Runner - Исполнитель тестов (Запуск тестов и выдача результата)
"""

from vector import Vector


class VectorTests(unittest.TestCase):

    def setUp(self):
        """Запускается перед каждым тестом"""
        # Создание тестового окружения (fixture)
        self.v1 = Vector(1, 2)
        self.v2 = Vector(2, 1)

    def tearDown(self):
        """Запускается после каждого теста"""
        # Очистка окружения (например, удаление файлов)
        del self.v1
        del self.v2


    def test_summa(self):
        """Суммирование векторов"""

        self.assertTrue(hasattr(self.v1, '__add__'), msg='Сложение не реализовано')

        v3 = self.v1 + self.v2

        self.assertIsInstance(v3, Vector, msg='Возвращаемый тип не Vector')

        result = (v3.x, v3.y)
        right = (3, 3)

        self.assertTupleEqual(right, right, msg='Неверный результат сложения')

    def test_minus(self):
        """Разность векторов"""

        self.assertTrue(hasattr(self.v1, '__sub__'), msg='Сложение не реализовано')

        v3 = self.v1 - self.v2

        self.assertIsInstance(v3, Vector, msg='Возвращаемый тип не Vector')

        result = (v3.x, v3.y)
        right = (-1, 1)

        self.assertTupleEqual(right, right, msg='Неверный результат вычитания')
