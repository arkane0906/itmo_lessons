# ORM - Object Relational Mapping
# Объектно-реляционное отображение

# PonyORM

from datetime import date

from pony.orm import (
    Database, Required, Optional, Set, db_session, sql_debug, show, select
)


db = Database()


sql_debug(True)


class Person(db.Entity):
    firstname = Required(str, 50)
    lastname = Required(str, 50)
    phone = Required(str, 20)
    birthday = Optional(date)
    address = Optional('Address')
    points = Optional(int)


class Address(db.Entity):
    city = Required(str, 50)
    street = Required(str, 100)
    build = Required(str, 50)
    flat = Required(str, 20)
    persons = Set('Person')


# db.bind(provider='sqlite', filename=':memory:')

db.bind(provider='postgres', user='arkane', password='0906422',
        host='localhost', database='python_orm')

db.generate_mapping(create_tables=True)

with db_session:
    address = Address(
        city='Санкт-Петербург',
        street='Прибрежная ул.',
        build='4',
        flat='278'
    )
    person = Person(
        firstname='Василий',
        lastname='Теркин',
        phone='+79819584423',
        birthday=date.today(),
        points=12,
        address=address
    )


with db_session:
    person = Person[1]  # Выборка по pk
    show(person)
    # Показать связанный объект (не грузится, если не надо (ленивый))
    show(person.address)


# with db_session:
#     # Получить по firstname 1 объект
#     person = Person.get(firstname='Василий')
#     show(person)


with db_session:
    persons = select(p for p in Person if p.firstname == 'Василий')
    show(persons)

# почитать про Entity hooks
