# Декораторы

# def tst(*args, **kwargs):
# 	print(type(args))
# 	print(type(kwargs))
# 	print(args)
# 	print(kwargs)

# arguments = [1, 2, 3, 4, 5]
# keyword_arguments = {
# 	'user': 'Arakne',
# 	'password': '12345678'
# }

# tst(*arguments, **keyword_arguments)

# def func():

# 	def wrapper():
# 		pass
# 	return wrapper

# f = func()
# print(type(f), f)

# from urllib.request import urlopen

# def page(url):
# 	def get():
# 		return urlopen(url).read()
# 	return get

# python = page('https://python.org')
# print(type(python), python())  # python - ссылка, python() - выполнение

import time


def benchmark(func):
	def wrapper(*args, **kwargs):
		start = time.time()
		result = func(*args, **kwargs)
		work_time = time.time() - start
		print('Функция "{}" выполнилась за {:f} секунд'.format(
			func.__name__, work_time
		))
		return result
	return wrapper

@benchmark
def factorial(n):
	f = 1

	for i in range(1, n + 1):
		f += i

	return f

print(factorial(999999))

"""
ЧИТАТЬ ПРО ЗАМЫКАНИЕ В PYTHON
"""
