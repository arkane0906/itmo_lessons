from datetime import datetime
from functools import reduce, wraps
import pickle
import time


# def decorator(func):
# 	# Является функцией
# 	# Принимает единственный аргумент - дек-ую функцию
# 	# Должен вернуть функцию-обертку

# 	def wrapper(*args, **kwargs):
# 		# Внутри обертки необходимо вызвать дек-ую функцию
# 		# Действия до вызова
# 		result = func(*args, **kwargs)
# 		# Действия после вызова
# 		return result

# 	return wrapper


# @decorator
# def tst():
# 	pass


# tst()


def benchmark(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        work_time = time.time() - start
        print('Функция "{}" выполнилась за {:f} микросекунд'.format(
            func.__name__, work_time * 1e6
        ))
        return result
    return wrapper


def cache(func):
    """
    Готовый находится в functools.lru_cache
    """
    memory = {}

    @wraps(func)
    def wrapper(*args, **kwargs):
        key = pickle.dumps((args, sorted(kwargs.items())))
        if key not in memory:
            memory[key] = func(*args, **kwargs)
        return memory[key]

    return wrapper


# Декораторы с параметрами

"""
Функция log принимаетпараметры декоратора и 
возвращает декоратор
"""


def log(filename):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)

            template = """
[{now:%Y-%m-%d %H:%M:%S}] Function: "{func}" called with:
    -> positional arguments:  {args}
    -> keywors arguments: 	  {kwargs}
Result: {result}
"""gf()
            with open(filename, 'a') as f:
                f.write(template.format(
                        now=datetime.now(),
                        func='.'.join([func.__module__, func.__name__]),
                        args=args,
                        kwargs=kwargs,
                        result=result
                        ))
            return result
        return wrapper
    return decorator


@log('log.txt')
@benchmark
@cache
def factorial(n):
    return reduce(lambda f, x: f * x, range(1, n + 1))


# print(factorial(15))

# print(factorial)

print(factorial(99), factorial(99))
