// Переменные

var a = 1;

// Типы данных
// Скалярные: number, boolean, string
// Составные: array, object
// Специальные: null, undefined
// Значение NaN (not a number)

// Операторы: почти все как в питоне
// === !== - операторы идентичности

console.log('0' == 0, '0' === 0);
console.log('string' / 8);


if (a === 1) {
  console.info('a = 1');
} else if (a === 2) {
  console.info('a = 2');
} else {
  console.info('a not 1 or 2');
}


// var choice = prompt();
//
// switch (choice) {
//   case '1':
//     console.log('Команда 1');
//     break;
//   default:
//     console.log('Команда не найдена!');
// }


// Циклы

var arr = [1, 2, 3, 4, 5];

for (var i = 0; i < 10; i++) {
  console.log(i);
}


for (var i = 0; i < arr.length; i++) {
  console.log(
    'Элемент с индексом ' + i + ' со значением ' + arr[i]
  );
}


// while () {}


var person = { // Plain Object
  id: 1,
  name: 'Linus Torvalds',
  skills: ['Linux', 'C++']
};

for (prop in person) {
  console.log(prop, person[prop]);
}

for (i in arr) {
  console.info(i, arr[i]);
}









