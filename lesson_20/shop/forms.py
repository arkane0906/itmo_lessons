from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, HiddenField, PasswordField
from wtforms.validators import InputRequired, Email


class CustomerForm(FlaskForm):
    id = HiddenField()
    email = StringField('E-mail', validators=[InputRequired(), Email()])
    password = PasswordField('Пароль', validators=[InputRequired()])
    name = StringField('Имя', validators=[InputRequired()])
    country = StringField('Страна')
    address = StringField('Адрес')
